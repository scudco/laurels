require 'sinatra/base'

module Leaves
  class Server < Sinatra::Base
    before do
      if request.accept.include?('*/*') || request.accept.include?('application/vnd.laurels.leaves+json')
        content_type 'application/vnd.laurels.leaves+json'
      else
        halt 406
      end
    end

    options '/' do
      headers 'Allow' => "GET, HEAD, OPTIONS"
    end

    get '/' do
      {}.to_json
    end

    [:delete, :patch, :post, :put].each do |method|
      send(method, '/') { halt 405 }
    end
  end
end
