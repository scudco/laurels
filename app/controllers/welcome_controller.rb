class WelcomeController < ApplicationController
  def leaves
    render :layout => 'application'
  end

  def marketing
    response.headers['Cache-Control'] = "public, must-revalidate, max-age=#{1.day.seconds}"
    render :layout => 'marketing'
  end
end
