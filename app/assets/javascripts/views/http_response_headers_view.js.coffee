window.HTTPResponseHeadersView = Backbone.View.extend
  tagName: 'table'
  className: 'http-response-headers'
  template: JST['templates/http_response_headers']

  initialize: ->
    _.bindAll @, 'render', 'renderHeaders'

  render: ->
    $(@el).html @template()
    $('.http-console-response .http-response-line').after(@el)
    @renderHeaders()
    @

  renderHeaders: ->
    tableBody = @$('tbody')
    tableBody.empty()
    headers = @collection
    headers.each (header)->
      view = new HTTPHeaderView
        model: header
        collection: headers
        type: 'response'
      tableBody.append(view.render().el)
    @
