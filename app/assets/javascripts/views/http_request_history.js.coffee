window.HTTPRequestHistoryView = Backbone.View.extend
  tagName: 'div'
  className: 'http-request-history'
  template: JST['templates/http_request_history']
  events:
    'click .label': 'loadRequest'

  initialize: ->
    _.bindAll @, 'render', 'loadRequest', 'renderRequests'
    @console = @options.console
    @collection = @options.collection
    @collection.bind('add', @renderRequests)
    @collection.bind('remove', @renderRequests)

  render: ->
    $(@el).html @template()
    @

  renderRequests: ->
    self = @
    list = @$('p').empty()
    @collection.each (request)->
      list.append $('<span class="label clickable"/>').addClass(request.labelClass).text(request.summary)
      list.append ' '
    @

  loadRequest: (evt)->
    index = @$('.label').toArray().indexOf(evt.target)
    if index >= 0
      @console.loadRequest(@collection.at(index))
    @
