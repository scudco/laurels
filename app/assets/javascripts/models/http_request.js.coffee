window.HTTPRequest = Backbone.Model.extend
  defaults:
    verb: 'OPTIONS'
    location: '/'
    body: ''

  initialize: ->
    _.bindAll @, 'setupDefaultHeaders', 'fullJSON', 'labelClass', 'summary', 'sendRequest'
    @headers = new HTTPHeaders
    @setupDefaultHeaders()

  setupDefaultHeaders: ->
    @headers.add new HTTPHeader
      name: 'Host'
      value: window.location.host
    @headers.add new HTTPHeader
      name: 'Accept'
      value: '*/*'
    @

  fullJSON: ->
    json = @toJSON()
    json['headers'] = @headers.toJSON()
    json

  labelClass: ->
    HTTPResponse.labelClass(@response.statusCode())

  summary: ->
    @get('verb') + ' ' + @get('location') + ' - ' + @response.statusCode()

  sendRequest: (callback,errback)->
    @set sentAt: (new Date()).toString()
    self = @
    params =
      contentType: 'application/json'
      data: JSON.stringify @fullJSON()
      dataType: 'json'
      error: (err)->
        errback(err)
      processData: false
      success: (data)->
        self.response = new HTTPResponse(data)
        callback(self.response)
      type: 'POST'
      url: Laurels.httpConsoleURL
    $.ajax(params)
    @

window.HTTPRequest.verbs = ['OPTIONS','GET','DELETE','PATCH','POST','PUT']
window.HTTPRequest.safeVerbs = ['OPTIONS','GET']
window.HTTPRequest.idempotentVerbs = ['OPTIONS','GET', 'DELETE', 'PUT']
window.HTTPRequest.acceptsBodyVerbs = ['PATCH','POST','PUT']
