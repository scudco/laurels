namespace :unicorn do
  namespace :server do
    desc "Start the dev server"
    task :start do
      `bundle exec unicorn -c config/unicorn_local.rb -D`
    end

    desc "Stop the dev server"
    task :stop do
      `kill \`cat tmp/pids/unicorn.pid\``
    end

    desc "Stop and then restart the dev server"
    task :restart => [:stop, :start]

    desc "Gracefully reload the app"
    task :reload do
      `kill -HUP \`cat tmp/pids/unicorn.pid\``
    end
  end
end
