window.HTTPHeaders = Backbone.Collection.extend
  model: HTTPHeader

window.HTTPHeaders.requestHeaders = [
  {
    name: 'Accept'
    description: 'Content-Types that are acceptable'
    example: 'text/plain'
  },
  {
    name: 'Accept-Charset'
    description: 'Character sets that are acceptable'
    example: 'utf-8'
  },
  {
    name: 'Accept-Encoding'
    description: 'Acceptable encodings.'
    example: '<compress | gzip | deflate | sdch | identity>'
  },
  {
    name: 'Accept-Language'
    description: 'Acceptable languages for response'
    example: 'en-US'
  },
  {
    name: 'Authorization'
    description: 'Authentication credentials for HTTP authentication'
    example: 'Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=='
  },
  {
    name: 'Cache-Control'
    description: 'Used to specify directives that MUST be obeyed by all caching mechanisms along the request/response chain'
    example: 'no-cache'
  },
  {
    name: 'Connection'
    description: 'What type of connection the user-agent would prefer'
    example: 'close'
  },
  {
    name: 'Cookie'
    description: 'an HTTP cookie previously sent by the server with Set-Cookie'
    example: '$Version=1; Skin=new;'
  },
  {
    name: 'Content-Length'
    description: 'The length of the request body in octets (8-bit bytes)'
    example: '348'
  },
  {
    name: 'Content-MD5'
    description: 'A Base64-encoded binary MD5 sum of the content of the request body'
    example: 'Q2hlY2sgSW50ZWdyaXR5IQ=='
  },
  {
    name: 'Content-Type'
    description: 'The mime type of the body of the request (used with PATCH, POST, and PUT requests)'
    example: 'application/x-www-form-urlencoded'
  },
  {
    name: 'Date'
    description: 'The date and time that the message was sent'
    example: 'Tue, 15 Nov 1994 08:12:31 GMT'
  },
  {
    name: 'Expect'
    description: 'Indicates that particular server behaviors are required by the client'
    example: '100-continue'
  },
  {
    name: 'From'
    description: 'The email address of the user making the request'
    example: 'user@example.com'
  },
  {
    name: 'Host'
    description: 'The domain name of the server (for virtual hosting), mandatory since HTTP/1.1'
    example: 'en.wikipedia.org'
  },
  {
    name: 'If-Match'
    description: 'Only perform the action if the client supplied entity matches the same entity on the server. This is mainly for methods like PUT to only update a resource if it has not been modified since the user last updated it.'
    example: '"737060cd8c284d8af7ad3082f209582d"'
  },
  {
    name: 'If-Modified-Since'
    description: 'Allows a 304 Not Modified to be returned if content is unchanged'
    example: 'Sat, 29 Oct 1994 19:43:31 GMT'
  },
  {
    name: 'If-None-Match'
    description: 'Allows a 304 Not Modified to be returned if content is unchanged, see HTTP ETag'
    example: '"737060cd8c284d8af7ad3082f209582d"'
  },
  {
    name: 'If-Range'
    description: 'If the entity is unchanged, send me the part(s) that I am missing; otherwise, send me the entire new entity'
    example: '"737060cd8c284d8af7ad3082f209582d"'
  },
  {
    name: 'If-Unmodified-Since'
    description: 'Only send the response if the entity has not been modified since a specific time.'
    example: 'Sat, 29 Oct 1994 19:43:31 GMT'
  },
  {
    name: 'Max-Forwards'
    description: 'Limit the number of times the message can be forwarded through proxies or gateways.'
    example: '10'
  },
  {
    name: 'Pragma'
    description: 'Implementation-specific headers that may have various effects anywhere along the request-response chain.'
    example: 'no-cache'
  },
  {
    name: 'Proxy-Authorization'
    description: 'Authorization credentials for connecting to a proxy.'
    example: 'Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=='
  },
  {
    name: 'Range'
    description: 'Request only part of an entity. Bytes are numbered from 0.'
    example: 'bytes=500-999'
  },
  {
    name: 'Referer'
    description: 'This is the address of the previous web page from which a link to the currently requested page was followed. (The word “referrer” is misspelled in the RFC as well as in most implementations.)'
    example: 'http://en.wikipedia.org/wiki/Main_Page'
  },
  {
    name: 'TE'
    description: 'The transfer encodings the user agent is willing to accept: the same values as for the response header Transfer-Encoding can be used, plus the "trailers" value (related to the "chunked" transfer method) to notify the server it accepts to receive additional headers (the trailers) after the last, zero-sized, chunk.'
    example: 'trailers, deflate'
  },
  {
    name: 'Upgrade'
    description: 'Ask the server to upgrade to another protocol.'
    example: 'HTTP/2.0, SHTTP/1.3, IRC/6.9, RTA/x11'
  },
  {
    name: 'User-Agent'
    description: 'The user agent string of the user agent'
    example: 'Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)'
  },
  {
    name: 'Via'
    description: 'Informs the server of proxies through which the request was sent.'
    example: '1.0 fred, 1.1 nowhere.com (apache/1.1)'
  },
  {
    name: 'Warning'
    description: 'A general warning about possible problems with the entity body.'
    example: '199 Miscellaneous warning'
  }
]

window.HTTPHeaders.responseHeaders = [
  {
    name: 'Accept-Ranges'
    description: 'What partial content range types this server supports'
    example: 'bytes'
  },
  {
    name: 'Age'
    description: 'The age the object has been in a proxy cache in seconds'
    example: '12'
  },
  {
    name: 'Allow'
    description: 'Valid actions for a specified resource. To be used for a 405 Method not allowed'
    example: 'GET, HEAD'
  },
  {
    name: 'Cache-Control'
    description: 'Tells all caching mechanisms from server to client whether they may cache this object. It is measured in seconds'
    example: 'max-age=3600'
  },
  {
    name: 'Connection'
    description: 'Options that are desired for the connection'
    example: 'close'
  },
  {
    name: 'Content-Encoding'
    description: 'The type of encoding used on the data. See HTTP compression.'
    example: 'gzip'
  },
  {
    name: 'Content-Language'
    description: 'The language the content is in'
    example: 'da'
  },
  {
    name: 'Content-Length'
    description: 'The length of the response body in octets (8-bit bytes)'
    example: '348'
  },
  {
    name: 'Content-Location'
    description: 'An alternate location for the returned data'
    example: '/index.htm'
  },
  {
    name: 'Content-MD5'
    description: 'A Base64-encoded binary MD5 sum of the content of the response'
    example: 'Q2hlY2sgSW50ZWdyaXR5IQ=='
  },
  {
    name: 'Content-Disposition'
    description: 'An opportunity to raise a "File Download" dialogue box for a known MIME type'
    example: 'attachment; filename=fname.ext'
  },
  {
    name: 'Content-Range'
    description: 'Where in a full body message this partial message belongs'
    example: 'bytes 21010-47021/47022'
  },
  {
    name: 'Content-Type'
    description: 'The mime type of this content'
    example: 'text/html; charset=utf-8'
  },
  {
    name: 'Date'
    description: 'The date and time that the message was sent'
    example: 'Tue, 15 Nov 1994 08:12:31 GMT'
  },
  {
    name: 'ETag'
    description: 'An identifier for a specific version of a resource, often a message digest'
    example: '"737060cd8c284d8af7ad3082f209582d"'
  },
  {
    name: 'Expires'
    description: 'Gives the date/time after which the response is considered stale'
    example: 'Thu, 01 Dec 1994 16:00:00 GMT'
  },
  {
    name: 'Last-Modified'
    description: 'The last modified date for the requested object, in RFC 2822 format'
    example: 'Tue, 15 Nov 1994 12:45:26 GMT'
  },
  {
    name: 'Link'
    description: 'Used to express a typed relationship with another resource, where the relation type is defined by RFC 5988'
    example: '</feed>; rel="alternate"'
  },
  {
    name: 'Location'
    description: 'Used in redirection, or when a new resource has been created.'
    example: 'http://www.w3.org/pub/WWW/People.html'
  },
  {
    name: 'P3P'
    description: 'This header is supposed to set P3P policy, in the form of P3P:CP="your_compact_policy". However, P3P did not take off, most browsers have never fully implemented it, a lot of websites set this header with fake policy text, that was enough to fool browsers the existence of P3P policy and grant permissions for third party cookies.'
    example: 'CP="This is not a P3P policy! See http://www.google.com/support/accounts/bin/answer.py?hl=en&answer=151657 for more info."'
  },
  {
    name: 'Pragma'
    description: 'Implementation-specific headers that may have various effects anywhere along the request-response chain.'
    example: 'no-cache'
  },
  {
    name: 'Proxy-Authenticate'
    description: 'Request authentication to access the proxy.'
    example: 'Basic'
  },
  {
    name: 'Refresh'
    description: 'Used in redirection, or when a new resource has been created. This refresh redirects after 5 seconds. This is a proprietary, non-standard header extension introduced by Netscape and supported by most web browsers.'
    example: '5; url=http://www.w3.org/pub/WWW/People.html'
  },
  {
    name: 'Retry-After'
    description: 'If an entity is temporarily unavailable, this instructs the client to try again after a specified period of time.'
    example: '120'
  },
  {
    name: 'Server'
    description: 'A name for the server'
    example: 'Apache/1.3.27 (Unix) (Red-Hat/Linux)'
  },
  {
    name: 'Set-Cookie'
    description: 'an HTTP cookie'
    example: 'UserID=JohnDoe; Max-Age=3600; Version=1'
  },
  {
    name: 'Strict-Transport-Security'
    description: 'A HSTS Policy informing the HTTP client how long to cache the HTTPS only policy and whether this applies to subdomains.'
    example: 'max-age=16070400; includeSubDomains'
  },
  {
    name: 'Trailer'
    description: 'The Trailer general field value indicates that the given set of header fields is present in the trailer of a message encoded with chunked transfer-coding.'
    example: 'Max-Forwards'
  },
  {
    name: 'Transfer-Encoding'
    description: 'The form of encoding used to safely transfer the entity to the user. Currently defined methods are: chunked, compress, deflate, gzip, identity.'
    example: 'chunked'
  },
  {
    name: 'Vary'
    description: 'Tells downstream proxies how to match future request headers to decide whether the cached response can be used rather than requesting a fresh one from the origin server.'
    example: '*'
  },
  {
    name: 'Via'
    description: 'Informs the client of proxies through which the response was sent.'
    example: '1.0 fred, 1.1 nowhere.com (Apache/1.1)'
  },
  {
    name: 'Warning'
    description: 'A general warning about possible problems with the entity body.'
    example: '199 Miscellaneous warning'
  },
  {
    name: 'WWW-Authenticate'
    description: 'Indicates the authentication scheme that should be used to access the requested entity.'
    example: 'Basic'
  }
]
