window.HTTPRequestView = Backbone.View.extend
  tagName: 'div'
  className: 'http-request'
  template: JST['templates/http_request']
  events:
    'click .send-request' : 'sendRequest'
    'click .reset-request': 'resetRequest'
    'change .http-request-verb': 'toggleVerbSpecificControls'

  initialize: ->
    _.bindAll @, 'sendRequest', 'resetRequest', 'toggleVerbSpecificControls', 'toggleRequestBody', 'toggleVerbLabels', 'updateModel', 'hideActions'
    @history = @options.history
    @model = @options.model
    @model.bind('change:sentAt', @hideActions)
    @headersView = new HTTPRequestHeadersView
      collection: @model.headers

  render: ->
    $(@el).html @template()
    $('.http-console-request').html(@el)
    $('.http-console-response').empty()
    @$('.http-request-line .verb-label').popover()
    @headersView.render()
    @toggleVerbSpecificControls()
    if @model.get('sentAt')?
      @hideActions()
      @responseView = new HTTPResponseView
        response: @model.response
      @responseView.render()
    @

  updateModel: ->
    @model.set
      verb: @$('.http-request-verb').val()
      location: @$('.http-request-location').val()
      body: @$('.http-request-body').val()
    @

  sendRequest: ->
    @updateModel()
    spinner = new Spinner(Spinner.defaults).spin($('.http-console-response')[0])
    self = @
    self.model.sendRequest (response)->
      console.log 'success'
      self.response = response
      self.responseView = new HTTPResponseView
        response: self.response
      self.responseView.render()
      spinner.stop()
      self.history.add self.model
    , (err)->
      spinner.stop()
      console.log err
    @

  resetRequest: ->
    @model = new HTTPRequest
    @headersView = new HTTPRequestHeadersView
      collection: @model.headers
    @responseView.remove() if @responseView?
    @render()
    @

  toggleVerbSpecificControls: ->
    @toggleVerbLabels()
    @toggleRequestBody()
    @

  toggleRequestBody: ->
    verb = @$('.http-request-verb').val()
    if (_.indexOf HTTPRequest.acceptsBodyVerbs, verb) >= 0
      @$('.http-request-body').show()
    else
      @$('.http-request-body').hide()

  toggleVerbLabels: ->
    verb = @$('.http-request-verb').val()
    @$('.verb-label').each (index,label)->
      $(label).removeClass('success notice').data('popover').enabled = false
      true

    if (_.indexOf HTTPRequest.safeVerbs, verb) >= 0
      @$('.verb-label.safe')
        .addClass('success')
        .data('popover').enabled = true

    if (_.indexOf HTTPRequest.idempotentVerbs, verb) >= 0
      @$('.verb-label.idempotent')
        .addClass('notice')
        .data('popover').enabled = true

  hideActions: ->
    @$('tfoot, button.remove-header').remove()
    @$('.http-request-line :input').remove()
    @$('.http-request-line').prepend(@model.get('verb') + ' ' + @model.get('location'))

    if (_.indexOf HTTPRequest.acceptsBodyVerbs, @model.get('verb')) >= 0
      @$('textarea.http-request-body').replaceWith($('<pre />').text(@model.get('body')))
    else
      @$('textarea.http-request-body').remove()

    timestamp = $('<abbr />').attr('title', @model.get('sentAt')).text(@model.get('sentAt')).relatizeDate()
    @$('.actions')
      .empty()
      .append('sent ')
      .append(timestamp)
      .append( $('<p />').html('<button class="new-request btn primary small">start a new request</button>') )
