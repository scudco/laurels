window.HTTPHeaderView = Backbone.View.extend
  tagName: 'tr'
  className: 'http-header'
  template: JST['templates/http_header']

  events:
    'click .edit-header': 'editHeader'
    'click .remove-header': 'removeHeader'

  initialize: ->
    _.bindAll @, 'editHeader', 'removeHeader'
    @collection = @options.collection
    @type = @options.type
    @

  render: ->
    $(@el).html @template(@model.toJSON())
    popoverFunction = if @type == 'response'
      @model.tooltipForResponseHeader
    else
      @model.tooltipForRequestHeader
    @$('td:first').popover
      content: popoverFunction
    @

  editHeader: ->
    console.log 'editHeader'
    @

  removeHeader: ->
    @collection.remove(@model)
    @
