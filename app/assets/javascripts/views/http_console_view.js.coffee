window.HTTPConsoleView = Backbone.View.extend
  tagName: 'div'
  className: 'http-console'
  template: JST['templates/http_console']
  events:
    'click .new-request': 'newRequest'

  initialize: ->
    _.bindAll @, 'render', 'newRequest', 'loadRequest'
    @consoleID = @options.consoleID
    @request = @options.request
    @history = new HTTPRequests
    @historyView = new HTTPRequestHistoryView
      collection: @history
      console: @

  render: ->
    @requestView = new HTTPRequestView
      model: @request
      history: @history

    $(@el).html @template()
    $(@consoleID).empty().html @el
    @$('.http-request-history').html @historyView.render().el
    @historyView.renderRequests()
    @requestView.render()
    @

  newRequest: ->
    @loadRequest()

  loadRequest: (request)->
    @request = request || new HTTPRequest
    @requestView = new HTTPRequestView
      model: @request
      history: @history
    @requestView.render()
    @
