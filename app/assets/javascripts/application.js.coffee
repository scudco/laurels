# This is a manifest file that'll be compiled into including all the files listed below.
# Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
# be included in the compiled file accessible from http://example.com/assets/application.js
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# the compiled file.
#
#= require jquery
#= require jquery_ujs
#= require backbone-rails
#= require ./vendor/bootstrap-twipsy
#= require_tree ./vendor
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./collections
#= require_tree ./views
#= require_tree ./routers
#= require_tree .

Spinner.defaults =
  lines: 12
  length: 7
  width: 4
  radius: 10
  color: '#000'
  speed: 1
  trail: 60

$ = jQuery
$ ->
  new Laurels
  Backbone.history.start pushState: true
