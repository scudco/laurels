window.Laurels = Backbone.Router.extend
  routes:
    '': 'home'

  initialize: ->
    @httpConsoleView = new HTTPConsoleView
      consoleID: '#console'
      request: new HTTPRequest

  home: ->
    @httpConsoleView.render()

window.Laurels.httpConsoleURL = '/http_console'
