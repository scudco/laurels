window.HTTPHeader = Backbone.Model.extend
  defaults:
    isEditable: true
  initialize: ->
    _.bindAll @, 'tooltipForRequestHeader', 'tooltipForResponseHeader'

  tooltipForRequestHeader: ->
    self = @
    header = _.detect HTTPHeaders.requestHeaders, (header)->
      header.name == self.get('name')
    if header?
      header['description']
    else
      'This is a custom header or one not documented in Laurels'

  tooltipForResponseHeader: ->
    self = @
    header = _.detect HTTPHeaders.responseHeaders, (header)->
      header.name == self.get('name')
    if header?
      header['description']
    else
      'This is a custom header or one not documented in Laurels'
