require 'leaves'

module Laurels
  module RouteConstraints
    class HTMLRequest
      def self.matches?(request)
        request.accept =~ /html/
      end
    end
  end
end

Laurels::Application.routes.draw do

  match "landing", :to => 'welcome#index'

  constraints(Laurels::RouteConstraints::HTMLRequest) do
    match "/", :to => 'welcome#leaves', :constraints => { :subdomain => 'leaves' }
  end
  mount Leaves::Server, :at => "/", :constraints => { :subdomain => 'leaves' }

  root :to => 'welcome#marketing'

end
