window.HTTPResponseView = Backbone.View.extend
  tagName: 'div'
  className: 'http-response'
  template: JST['templates/http_response']

  initialize: ->
    _.bindAll @, 'render'
    @model = @options.response
    @headersView = new HTTPResponseHeadersView
      collection: @model.headers

  render: ->
    $(@el).html @template(@model.toJSON())
    @$('.status-label').popover()
    $('.http-console-response').html(@el)
    @headersView.render()
    @
