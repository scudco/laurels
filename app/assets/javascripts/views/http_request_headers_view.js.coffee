window.HTTPRequestHeadersView = Backbone.View.extend
  tagName: 'table'
  className: 'http-request-headers'
  template: JST['templates/http_request_headers']

  events:
    'click .add-header': 'addHeader'
    'keypress .new-http-request-header-name, .new-http-request-header-value': 'addHeader'
    'change .new-http-request-header-name': 'renderHelpText'

  initialize: ->
    _.bindAll @, 'render', 'addHeader', 'renderHeaders', 'renderHelpText'
    @collection.bind('add', @renderHeaders)
    @collection.bind('remove', @renderHeaders)

  render: ->
    $(@el).html @template()
    @renderHelpText()
    $('.http-console-request .http-request-line').after(@el)
    @renderHeaders()
    @

  addHeader: (evt)->
    return @ unless(evt.type == 'click' || evt.keyCode == 13)
    header = new HTTPHeader
      name: @$('.new-http-request-header-name').val()
      value: @$('.new-http-request-header-value').val()
    @collection.add(header)
    @$('.new-http-request-header-name, .new-http-request-header-value').val('')
    @renderHelpText()
    @

  renderHeaders: ->
    tableBody = @$('tbody')
    tableBody.empty()
    headers = @collection
    headers.each (header)->
      view = new HTTPHeaderView
        model: header
        collection: headers
      tableBody.append(view.render().el)

  renderHelpText: (evt)->
    self = @
    val = @$('.new-http-request-header-name').val()
    header = _.detect HTTPHeaders.requestHeaders, (header)->
      header.name == val
    @$('.new-http-request-header-name').nextAll('.help-block').first().text(header['description'])
    @$('.new-http-request-header-value').nextAll('.help-block').first().text('ex: ' + header['example'])
    @
