window.HTTPResponse = Backbone.Model.extend
  initialize: ->
    _.bindAll @, 'setupDefaultHeaders', 'statusCode'
    @headers = new HTTPHeaders
    @setupDefaultHeaders()

  statusCode: ->
    @get('statusCode')

  setupDefaultHeaders: ->
    @headers.add new HTTPHeader
      name: 'Server'
      value: 'nginx/1.0.5'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'Date'
      value: (new Date()).toString()
      isEditable: false
    @headers.add new HTTPHeader
      name: 'Content-Type'
      value: 'application/vnd.laurels.leaves+json'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'Connection'
      value: 'keep-alive'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'Status'
      value: '200 OK'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'X-Frame-Options'
      value: 'sameorigin'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'X-XSS-Protection'
      value: '1; mode=block'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'Content-Length'
      value: '2'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'X-UA-Compatible'
      value: 'IE=Edge'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'ETag'
      value: "99914b932bd37a50b983c5e7c90ae93b"
      isEditable: false
    @headers.add new HTTPHeader
      name: 'Cache-Control'
      value: 'max-age=0, private, must-revalidate'
      isEditable: false
    @headers.add new HTTPHeader
      name: 'X-Runtime'
      value: '0.001440'
      isEditable: false
    @

window.HTTPResponse.labelClass = (statusCode)->
  if ( statusCode >= 100 && statusCode < 200 )
    'informational'
  else if ( statusCode >= 200 && statusCode < 300 )
    'success'
  else if ( statusCode >= 300 && statusCode < 400 )
    'redirection'
  else if ( statusCode >= 400 && statusCode < 500 )
    'client-error'
  else if ( statusCode >= 500 )
    'server-error'
  else
    ''
